require('dotenv').config();

// Middleware
const checkAuth = require('./checkAuth');

const _ = require('lodash');
const express = require('express');
const bodyParser = require('body-parser');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

// Initialize and connect to our monogodb
const { mongoose } = require('./db/mongoose');


// Data Schema 
const { DataSchema } = require('./models/dataSchema');
const { userSchema } = require('./models/user');

//Initializing the server
const app = express();
const PORT = 3000;


//Setting up the middleware
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


// ROUTES GO HERE

app.get('/', (req, res) => {
    res.json({ "message": "This database stores all the passwords of the accounts we are using. Yes this is an invitation to hack us." })
})


// Get all the Accounts
app.get('/getAccounts', checkAuth, (req, res) => {

    DataSchema.find()
        .then((DataSchema) => {
            // TODO: Add code to check if database is empty
            if (DataSchema.length < 1) {
                return res.status(400).send({ "message": "There are currently no accounts." })
            }
            res.send({ DataSchema })
        }).catch((err) => {
            res.status(500).send({ "message": "Unable to retrieve data from the database." })
        })

})


// Add One Single Account
app.post('/addAccount', checkAuth, (req, res) => {

    // Validate request
    if (!req.body) {
        return res.status(400).send({
            "message": "Request body is empty"
        })
    }
    // Creating a data entry
    const DataEntry = new DataSchema({
        website: req.body.website,
        account: req.body.account,
        apiKey: req.body.apiKey,
        addedBy: req.body.addedBy,
    })

    // Saving the data entry in the database
    DataEntry.save()
        .then((DataSchema) => {
            res.send({ DataSchema });
        }).catch((err) => {
            res.status(400).send({ "message": "Unable to save data to database." })
        })
})






// Get One Single Account
app.get('/getAccount/:website', checkAuth, (req, res) => {

    DataSchema.findOne({ website: req.params.website })
        .then((DataSchema) => {
            if (!DataSchema) {
                return res.status(400).send({ "message": "Website not found." })
            }
            res.send({ DataSchema });
        }).catch((err) => {
            if (err.kind === 'ObjectId') {
                return res.status(404).send({ "message": "Website not found" })
            }

            res.status(500).send({ "message": "Error retrieving website " + req.params.website })

        })

})



// Delete One Single Account
app.delete('/deleteAccount/:website', checkAuth, (req, res) => {
    DataSchema.findOneAndDelete({ website: req.params.website })
        .then((DataSchema) => {
            if (!DataSchema) {
                return res.status(404).send({ "message": "Website not found with name " + req.params.website });
            }
            res.send({ "message": "Account deleted successfully" })
        }).catch((err) => {
            if (err.kind === 'ObjectId' || err.name === 'NotFound') {
                return res.status(404).send({ "message": "Account not found with name " + req.params.website })
            }

            return res.status(500).send({ "message": "Could not delete account with name " + req.params.website });
        })
})


// Update One Single Account
app.put('/updateAccount/:website', checkAuth, (req, res) => {

    // Validate Request
    if (!req.body) {
        return res.status(400).send({ "message": "Request body is empty" })
    }


    DataSchema.findOneAndUpdate({ website: req.params.website }, {
        website: req.body.website,
        account: req.body.account,
        apiKey: req.body.apiKey,
        addedBy: req.body.addedBy
    }, { new: true })
        .then((DataSchema) => {
            if (!DataSchema) {
                return res.status(400).send({ "message": "Website not found with name " + req.params.website })
            }

            res.send({ DataSchema });
        }).catch((err) => {
            if (err.kind === 'ObjectId') {
                return res.status(404).send({ "message": "Website not found with name " + req.params.website })
            };

            return res.status(500).send({ "message": "Error updating website with " + req.params.website })
        })

})



// ---------------------- Routes for the user Authentication --------------------//
app.post('/signupUser', (req, res, next) => {

    userSchema.find({ email: req.body.email })
        .then(user => {
            if (user.length >= 1) {
                return res.status(409).send({ "message": "User already exists." })
            } else {

                bcrypt.hash(req.body.password, 10, (err, hash) => {
                    if (err) {
                        return res.status(500).json({ message: err })
                    } else {
                        const user = new userSchema({
                            _id: new mongoose.Types.ObjectId(),
                            email: req.body.email,
                            password: hash
                        });
                        user.save()
                            .then(result => {
                                res.status(201).send({ "message": "User Created" })
                            })
                            .catch(err => {
                                // console.log(err);
                                res.status(500).send({ "message": err })
                            });
                    }
                });

            }
        });

});

// Login route
app.post('/loginUser', (req, res, next) => {
    userSchema.find({ email: req.body.email }).exec()
        .then(user => {
            if (user.length < 1) {
                return res.status(401).send({ "message": "Auth failed" });
            }
            bcrypt.compare(req.body.password, user[0].password, (err, result) => {
                if (err) {
                    return res.status(401).send({ "message": "Auth failed" });
                }
                if (result) {
                    const token = jwt.sign({
                        email: user[0].email,

                    }, process.env.JWT_KEY,
                        {
                            expiresIn: "10m"
                        })
                    return res.status(200).send({
                        "message": "Auth successful",
                        "token": token
                    })
                }
                res.status(401).send({ "message": "Auth failed" });
            })
        })
        .catch(err => {
            res.status(500).send({ "message": "User not found " + err })
        })
});


app.delete('/deleteUser/:email', (req, res, next) => {
    userSchema.remove({ email: req.body.email })
        .then(result => {
            res.status(200).send({ "message": "User deleted with email " + req.params.email })
        }).catch(err => {
            res.status(500).send({ "message": err })
        })
});




// Start listening to the application on PORT

app.listen(PORT, () => {
    console.log(`Server started on PORT${PORT}`)
}); 