const env = require('../../config');
const mongoose = require('mongoose');


mongoose.Promise = global.Promise;

 
if (env.CONFIG === 0) {
    console.log("Connecting to development database.")
    //For developmemt purpose
    //To run on your machine you need to install mongodb server
    // mkdir ~/mongo-data
    // sudo ~./mongo/bin/mongod --dbpath ~/mongopath[mongo-data]
    mongoose.connect('mongodb://localhost:27017/MazeManagement',
        { useNewUrlParser: true }).then(() => {
            console.log("Successfully connected to development database")
        }).catch(err => {
            console.log('Could not connect to development database. Exiting now', err);
            process.exit();
        });
} else {

    // For production purpose
    mongoose.connect('mongodb://mongo:27017/SMAT',
        { useNewUrlParser: true }).then(() => {
            console.log("Successfully connected to database")
        }).catch(err => {
            console.log('Could not connect to database. Exiting now', err);
            process.exit();
        });
}



module.exports = { mongoose };