const mongoose = require('mongoose');



const DataSchema = mongoose.model('DataSchema', {
    website: {
        type: String,
        required: true,
        trim: true,
        lowercase:true,
    },
    account: {
        type: String,
        required: true,
        trim: true,
    },
    addedBy: {
        type: String,
        required: true,
        trim: true,
    },
    apiKey: {
        type: String,
        required: false,
        trim: true,
    },
    time: {
        type: Date,
        default: Date.now,
    }
})

module.exports = { DataSchema };